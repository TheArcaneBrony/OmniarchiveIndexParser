using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace OmniarchiveIndexParser;

public class Index {
    public static Index Instance;
    public DateTime LastUpdated = new();
    public Dictionary<string, List<McInfo>> VersionList;
    public string FileDate => $"{new FileInfo("index.json").LastWriteTimeUtc} UTC";

    public Index() {
        Instance = this;
    }

    public Index(bool populate = false) {
        Instance = this;
        if (populate) ParseVersionsRewrite();
    }

    public Dictionary<string, List<McInfo>> ParseVersionsRewrite(string url = "https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=1OCxMNQLeZJi4BlKKwHx2OlzktKiLEwFXnmCrSdAFwYQ&exportFormat=xlsx", bool UseCache = false) {
        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        if (VersionList != null) return VersionList;
        if (UseCache && File.Exists("index-new.json")) {
            Console.WriteLine($"Using last known state ({new FileInfo("index-new.json").LastWriteTimeUtc} UTC):");
            return JsonConvert.DeserializeObject<Dictionary<string, List<McInfo>>>(File.ReadAllText("index-new.json"));
        }

        Console.WriteLine("No last known state.");

        if (DateTime.Now.Subtract(LastUpdated).TotalMinutes > 10) {
            Console.WriteLine(DateTime.Now.Subtract(LastUpdated).TotalSeconds);
            LastUpdated = DateTime.Now;

            Console.WriteLine("DOWNLOADING INDEX...");
            try {
                WebClient wc = new();
                wc.DownloadFile(url, "index.xlsx");
            }
            catch {
                Console.WriteLine("Failed to download index, using last copy");
            }
        }
        else Console.WriteLine("Using cached index.");

        Console.WriteLine("START PARSING OMNIARCHIVE INDEX");
        Stopwatch indexParseTime = new Stopwatch();
        indexParseTime.Start();
        Dictionary<string, List<McInfo>> index = new Dictionary<string, List<McInfo>>();
        ExcelWorkbook workBook = new ExcelPackage(new FileInfo(@"index.xlsx")).Workbook;
        Type T = typeof(McInfo);
        
        for (int sheetNum = 1; sheetNum <= 4; sheetNum++) {
            Console.Write($"Parsing sheet {sheetNum} of {workBook.Worksheets.Count}: ");
            ExcelWorksheet csh = workBook.Worksheets[sheetNum];
            Console.Write(csh.Name);
            int row = 1;
            List<McInfo> vers = new List<McInfo>();
            if (!csh.Name.ToLower().Contains("java")) continue;
            List<string> sheetformat = new();
            var CellColor = "null";
            while (CellColor != "ff00ff" || csh.Cells["a" + row].Text.ToLower().Contains("note"))
            {
                CellColor = SheetTools.GetCellColor(csh.Cells, "B", row);
                if (CellColor == "000000") {
                    sheetformat = new();
                    char col = 'B';
                    Console.Write("Detecting columns... ");
                    while (csh.Cells[col + "" + row].Text != "") {
                        Console.Write($"{csh.Cells[col + "" + row].Text}, ");
                        sheetformat.Add(csh.Cells[col + "" + row].Text);
                        col++;
                    }
                    Console.WriteLine();
                }
                else if (CellColor == "434343") sheetformat[0] = csh.Cells["a" + (row)].Text;
                else {
                    char col = 'B';
                    McInfo mci = new() { Type = sheetformat[0], GamePlatform = csh.Name.Split(" ")[0], Environment = csh.Name.Split(" ")[1] };
                    for (int i = 1; i < sheetformat.Count; i++) {
                        //Console.Write($"{sheetformat[i]}: {csh.Cells[col + "" + row].Text}, ");
                        ExcelRange cell = csh.Cells[col + "" + row];
                        string celltext = cell.Text.Trim();
                        if (SheetTools.GetCellColor(csh.Cells, col + "", row) != "ff00ff") cell = csh.Cells[col + "" + (row - 1)];
                        T.GetProperty(sheetformat[i].Replace(" ", "").Replace("-", ""), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)?.SetValue(mci, SheetTools.GetHyperlinkedText(csh.Cells, cell.Address));
                        col++;
                    }

                    mci.Color = SheetTools.GetCellColor(csh.Cells, "H", row);
                    if (mci.ReleaseDate != "Released") vers.Add(mci);
                    //Console.Write('\n');
                }

                row++;
            }

            vers = vers.OrderBy(x => x.Type.IndexOfType()).ThenBy(x => x.Pvn).ThenBy(x => x.ReleaseDate).ToList();
            string kn = string.Join("", csh.Name.ToLower().Split(' ')[..2]);
            if (index.ContainsKey(kn)) index[kn].AddRange(vers);
            else index.Add(kn, vers);
            Console.WriteLine($" ({vers.Count} versions)");
        }

        File.WriteAllText("index-new.json", JsonConvert.SerializeObject(index, Formatting.Indented));
        indexParseTime.Stop();
        Console.WriteLine($"DONE PARSING OMNIARCHIVE INDEX IN {indexParseTime.ElapsedMilliseconds} MS");
        VersionList = index;
        return index;
    }
}