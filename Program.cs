﻿using OfficeOpenXml;

using System;
using System.Collections.Generic;

namespace OmniarchiveIndexParser
{
    public class Program
    {
        public static void Main() {
            Index index = new();
            //set excel parsing library's license context
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            //parse versions
            Console.WriteLine("Parse Versions (rewrite 2):");
            //index.ParseVersionsRewrite();
            new IndexHandler().ParseIndex();
            /*LogVersions(index);
            Console.WriteLine("Parse Versions (rewrite, cached):");
            index.ParseVersionsRewrite(UseCache: true);
            LogVersions(index);*/
        }
        private static void LogVersions(Index index)
        {
            int count = 0;
            foreach (KeyValuePair<string, List<McInfo>> list in index.VersionList)
            {
                count += list.Value.Count;
                Console.WriteLine($"{list.Key}: {list.Value.Count} entries");
            }
            Console.WriteLine($"Total: {count}");
        }
    }
}
