using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace OmniarchiveIndexParser;

public class IndexHandler
{
    public DateTime LastUpdated = new();
    public Dictionary<string, List<McInfo>> VersionList;
    public string FileDate => $"{new FileInfo("index.json").LastWriteTimeUtc} UTC";

    public Dictionary<string, List<McInfo>> ParseIndex(
        string url =
            "https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=1OCxMNQLeZJi4BlKKwHx2OlzktKiLEwFXnmCrSdAFwYQ&exportFormat=xlsx",
        bool UseCache = false)
    {
        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        if (VersionList != null) return VersionList;
        if (UseCache && File.Exists("index-new.json"))
        {
            Console.WriteLine($"Using last known state ({new FileInfo("index-new.json").LastWriteTimeUtc} UTC):");
            return JsonConvert.DeserializeObject<Dictionary<string, List<McInfo>>>(File.ReadAllText("index-new.json"));
        }

        Console.WriteLine("No last known state.");

        if (DateTime.Now.Subtract(LastUpdated).TotalMinutes > 10)
        {
            Console.WriteLine(DateTime.Now.Subtract(LastUpdated).TotalSeconds);
            LastUpdated = DateTime.Now;

            Console.WriteLine("DOWNLOADING INDEX...");
            try
            {
                WebClient wc = new();
                wc.DownloadFile(url, "index.xlsx");
            }
            catch
            {
                Console.WriteLine("Failed to download index, using last copy");
            }
        }
        else Console.WriteLine("Using cached index.");

        Console.WriteLine("START PARSING OMNIARCHIVE INDEX");
        Stopwatch indexParseTime = new Stopwatch();
        indexParseTime.Start();
        Dictionary<string, List<McInfo>> index = new Dictionary<string, List<McInfo>>();
        ExcelWorkbook workBook = new ExcelPackage(new FileInfo(@"index.xlsx")).Workbook;


        for (int sheetNum = 1; sheetNum <= 5; sheetNum++)
        {
            var timer = Stopwatch.StartNew();
            ExcelWorksheet csh = workBook.Worksheets[sheetNum];
            Console.Write($"Reading sheet {sheetNum} of {workBook.Worksheets.Count - 1}: {csh.Name}...");
            
            var td = ReadTable(csh);
            Console.Write($" Read {td.Count} rows in {timer.ElapsedMilliseconds} ms,");
            
            timer = Stopwatch.StartNew();
            var vers = ParseTable(td);
            Console.Write($" parsed in {timer.ElapsedMilliseconds} ms");
            
            timer = Stopwatch.StartNew();
            vers = vers.OrderBy(x => x.Type.IndexOfType()).ThenBy(x => x.Pvn).ThenBy(x => x.ReleaseDate).ToList();
            if (index.ContainsKey(csh.Name)) index[csh.Name].AddRange(vers);
            else index.Add(csh.Name, vers);
            Console.WriteLine($" and ordered in {timer.ElapsedMilliseconds} ms.");
        }

        File.WriteAllText("index-new.json", JsonConvert.SerializeObject(index, Formatting.Indented));
        indexParseTime.Stop();
        Console.WriteLine($"DONE PARSING OMNIARCHIVE INDEX IN {indexParseTime.ElapsedMilliseconds} MS");
        VersionList = index;
        return index;
    }

    public List<McInfo> ParseTable(List<Dictionary<string, string>> table)
    {
        Type T = typeof(McInfo);
        var props = T.GetProperties(BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
            .Where(x => x.GetMethod != null && x.SetMethod != null && x.GetCustomAttributes<FriendlyNameAttribute>().Any());
        List<McInfo> vers = new();
        foreach (var dictionary in table)
        {
            McInfo mci = new();
            foreach (var (key, value) in dictionary)
            {
                foreach (var prop in props)
                    if(prop.GetCustomAttributes<FriendlyNameAttribute>().Any(x=>x.Name == key))
                        prop.SetValue(mci, value);
            }

            vers.Add(mci);
        }

        return vers;
    }

    public List<Dictionary<string, string>> ReadTable(ExcelWorksheet csh)
    {
        //create empty list
        List<Dictionary<string, string>> data = new();
        //get header layout
        var layout = GetTableLayout(csh);
        //runtime data
        var c = csh.Cells;
        bool hasData = true;
        int row = 1;

        //fill data
        while (!IsEmptyRow(csh, ++row))
        {
            //Console.WriteLine(row);
            if (IsHeader(csh, row)) continue;
            Dictionary<string, string> verData = new();

            for (var i = 0; i < layout.Length; i++)
            {
                verData.Add(layout[i], GetMergedCell(csh, row, i + 1).Text);
            }

            verData["Release cycle"] = GetReleaseCycle(csh, row);
            verData.Add("Color", GetMergedCell(csh, row, 2).Style.Fill.BackgroundColor.Rgb[2..]);
            data.Add(verData);
        }

        return data;
    }

    private Dictionary<string, string[]> cache = new();

    public string[] GetTableLayout(ExcelWorksheet csh)
    {
        if (cache.ContainsKey(csh.Name)) return cache[csh.Name];
        List<string> columns = new() {"Release cycle"};
        char col = 'A';
        int row = 1;
        //Console.WriteLine("Detecting columns... ");
        while (csh.Cells[col++ + "" + row].Text != "" || col <= 'C')
        {
            columns.Add(csh.Cells[col + "" + row].Text);
        }

        var data = columns.Where(x => !string.IsNullOrEmpty(x)).ToArray();
        cache.Add(csh.Name, data);
        return data;
    }

    public string GetReleaseCycle(ExcelWorksheet csh, int row)
    {
        string releaseCycle = "none?";
        if (csh.Cells["A" + row].Merge) return GetMergedCell(csh, row, 1).Text;
        if (csh.Cells["A" + (row + 1)].Merge) return GetMergedCell(csh, row + 1, 1).Text;
        return releaseCycle;
    }

    public bool IsEmptyRow(ExcelWorksheet csh, int row)
    {
        return GetMergedCell(csh, row, 1).Text == GetMergedCell(csh, row, 2).Text &&
               GetMergedCell(csh, row, 3).Text == GetMergedCell(csh, row, 4).Text;
    }

    public ExcelRange GetMergedCell(ExcelWorksheet csh, int row, int col)
    {
        if (!csh.Cells[row, col].Merge) return csh.Cells[row, col];
        int cellid = csh.GetMergeCellId(row, col);
        var cellpos = csh.MergedCells[cellid - 1].Split(':')[0];
        return csh.Cells[cellpos];
    }

    public bool IsHeader(ExcelWorksheet csh, int row)
    {
        var tl = GetTableLayout(csh);
        return csh.Cells["B" + row].Text == tl[1];
    }
}